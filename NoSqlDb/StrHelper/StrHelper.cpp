#include<iostream>
#include<string>
#include"StrHelper.h"

#ifdef TEST_STRHELPER
int main()
{
	using namespace std;

	string s1 = "some string to test rtrim\n      ";
	string s2 = "        \ntesting ltrim";
	string s3 = "    lets test trim\n       ";

	cout << rtrim(s1);
	cout << ltrim(s2);
	cout << trim(s3);
}
#endif