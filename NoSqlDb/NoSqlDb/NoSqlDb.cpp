/////////////////////////////////////////////////////////////////////
// NoSqlDb.cpp - Test key/value pair in-memory database            //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// Source : Dr. Jim Fawcett, CSE687 - OOD, Spring 2017             //
/////////////////////////////////////////////////////////////////////
#include <iostream>
#include<chrono>
#include<ctime>
#include"../StrHelper/StrHelper.h"
#include "NoSqlDb.h"

using StrData = std::string;
using Key = NoSqlDb<StrData>::Key;
using Keys = NoSqlDb<StrData>::Keys;


#ifdef TEST_NOSQLDB
int main()
{
	std::cout << "\n  Creating and saving NoSqlDb elements with string data";
	std::cout << "\n -------------------------------------------------------\n";

	NoSqlDb<StrData> db = NoSqlDb<StrData>("../StrDB.xml");

	Element<StrData> file;
	file.name = "NoSqlDb.h";
	file.category = "header file";
	file.description = "Create and maintain a db";
	file.children.push_back("StrHelper.h");
	file.data = "NoSqlDb.h file path is the data";
	file.timeDate = timeString();
	db.addElem(file.name, file);

	file.clear();
	file.name = "XmlDocument.h";
	file.category = "header file";
	file.children.push_back("XmlElement.h");
	file.children.push_back("StrHelper.h");
	file.description = "Process Xml files";
	file.data = "XmlDocument.h file path is data";
	file.timeDate = timeString();
	db.addElem(file.name, file);

	file.clear();
	file.name = "XmlElement.h";
	file.category = "header file";
	file.description = "Process Xml Elements";
	file.children.push_back("StrHelper.h");
	file.data = "XmlElement.h file path is data";
	file.timeDate = timeString();
	db.addElem(file.name, file);
	db.saveDB("../StrDB.xml");


	std::cout << "\n  Retrieving elements from NoSqlDb<string>";
	std::cout << "\n ------------------------------------------\n";

	Keys keys = db.getKeys();
	for (Key key : keys)
	{
		std::cout << "\n  " << key << ":";
		std::cout << db.value(key).show();
	}
	std::cout << "\n\n";
}
#endif