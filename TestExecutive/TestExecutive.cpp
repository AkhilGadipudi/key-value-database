/////////////////////////////////////////////////////////////////////
// TestExecutive.cpp -  Demonstrates use of Dependency Analyzer	   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////

#include <fstream>
#include"../Analyzer/Executive.h"
#include"../TypeAnal/TypeAnal.h"
#include "../Logger/Logger.h"
#include"../DepAnalyzer/DepAnalyzer.h"
#include"../StrongCompAnal/StrongCompAnal.h"

int main(int argc, char* argv[])
{
	using namespace CodeAnalysis;
	using Rslt = Logging::StaticLogger<0>;
	CodeAnalysisExecutive exec;
	std::cout << "\n\nWritten in C++ using Visual Studio 2015 - Req #1\n";
	std::cout << "Used only C++ std lib for I/O and new-delete for heap-memory - Req #2\n";
	std::cout << "Created TypeTable, TypeAnal, DepAnslyzer, StrongCompAnal, TestExec as mentioned in Req\n"
		<< "Display funtionality incorporated into appropriate packages to display their contents	- Req #3\n";
	
	std::cout << "\n\nProcessing Command Line arguments		- Req #8";
	try {
		bool succeeded = exec.ProcessCommandLine(argc, argv);
		if (!succeeded)
		{
			return 1;
		}
		
		exec.startLogger(std::cout);
		exec.showCommandLineArguments(argc, argv);
		exec.setDisplayModes();
		exec.getSourceFiles();
		std::cout << "  \nAnalysing Code to create AST";
		exec.processSourceCode(false);
		std::cout << "  \nCode Analysis completed";

		// Start TypeAnalysis and get TypeTable
		TypeAnal ta;
		ta.doTypeAnal();
		TypeTable tt = ta.getTypeTable();
		tt.show();

		// Start Dependency analysis using TypeTable
		DepAnalyzer da(argv[1], tt);
		da.doDepAnal();
		da.show();
		da.showDB();
		da.toXml(argv[1]);

		// Start Strong Component Analysis using Dependency Table
		StrongCompAnal sc(da.getDepTable());
		sc.doStrongCompAnal();
		sc.show();		
		sc.toXml(argv[1]);

		std::cout << "\n This TestExecutive demonstrates all requirments     - Req #9\n";
	}
	catch (std::exception& except)
	{
		std::cout << "\n\n  caught exception in Executive::main: " + std::string(except.what()) + "\n\n";
		return 1;
	}
	return 0;
}
