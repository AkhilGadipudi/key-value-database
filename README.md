# Key Value Database
*	Designed and implemented NoSQL Database using key-value pairs.
*	Designed to contain any type of data using C++ templates.
*	Provided mechanisms for simple queries and compound queries over results of previous queries.
*	The DB can be persisted by saving contents to XML file and restored when required, DB can also be augmented using additional XML files.
*	Implemented in robust and reusable way, used in later projects like Type-Bases Dependency Analyzer
---
This was developed as part of CSE687 - Object Oriented Design, so some part of code is provided as a starter code by our professor [Dr. Jim Fawcett](http://ecs.syr.edu/faculty/fawcett/handouts/webpages/fawcettHome.htm)
