#pragma once
/////////////////////////////////////////////////////////////////////
// TypTable.h -  Type Table and its operations					   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017										//
/////////////////////////////////////////////////////////////////////
/*
Operation :
	Creates Type Table to hold information of each type encountered
	int the files analyzed and store where the type was created

Public Interface :
	TypeInfo ti();
	ti.filename , ti.typenamespace, ti.path // set type info

	TypeTable tt();
	tt.addType(typename, ti)   // add a type with type info
	tt.getValues(typename)		// get TypeInfo of a type
	tt.show()					// Print contents fo type table

Required Files :
	TypeTable.h, TypeTable.cpp
Build Command:
	devenv TypeTable.sln /rebuild debug

Maintenance History:
	ver 1.0 - March 7, 2017 - Initial release
*/

#include<unordered_map>
#include<vector>

class TypeInfo
{
public:
	std::string filename;
	std::string typenamespace;
	std::string path;
};

class TypeTable
{
	
	using Type = std::string;
	using typetable = std::unordered_map<Type, std::vector<TypeInfo>>;
private:
	 typetable tt_;
	 
public:
	void addType(Type type, TypeInfo ti);
	std::vector<TypeInfo> getValues(Type type);
	void show();
};
