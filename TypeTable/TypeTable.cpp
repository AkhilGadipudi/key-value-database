/////////////////////////////////////////////////////////////////////
// TypTable.cpp -  Type Table and its operations				   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////
#include<iostream>
#include<string>
#include<sstream>
#include<iomanip>
#include"TypeTable.h"

// add type and its typeinfo to taype table
void TypeTable::addType(Type type, TypeInfo ti)
{
	typetable::iterator item = tt_.find(type);
	if (item == tt_.end())
	{
		std::vector<TypeInfo> values;
		values.push_back(ti);
		tt_.emplace(type, values);
	}
	else
	{
		std::vector<TypeInfo> values = item->second;
		values.push_back(ti);
		item->second = values;
	}
}

// get typeinfo for a type
std::vector<TypeInfo> TypeTable::getValues(Type type)
{
	if (tt_.find(type) != tt_.end())
		return tt_.find(type)->second;
	else
		return std::vector<TypeInfo>();
}

// print taype table contents
void TypeTable::show()
{
	std::cout << " ==========  Type Table Contents  =============\n";
	std::ostringstream out;
	out.setf(std::ios::adjustfield, std::ios::left);
	out << std::setw(25) << "Type" << std::setw(25) << "Filename" << std::setw(25) << "Namespace" << "\n";
	out << std::setw(25) << "--------" << std::setw(25) << "------------" << std::setw(25) << "-------------" << "\n";
	std::cout << out.str();
	for (auto item : tt_)
	{		
		for (auto value : item.second)
		{	
			std::ostringstream out2;
			out2.setf(std::ios::adjustfield, std::ios::left);
			out2 << std::setw(25) << item.first;
			out2 << std::setw(25) << value.filename;
			out2 << std::setw(25) << value.typenamespace;
			out2 << "\n";
			std::cout << out2.str();
		}
	}
}

#ifdef TEST_TYPETABLE

int main()
{
	TypeInfo ti;
	ti.filename = "somefileaname";
	ti.typenamespace = "testnamespace";
	ti.path = "../somefilename";

	TypeTable tt;
	tt.addType("type", ti);
	if (tt.getValues().size() != 0)
		std::cout << "TypeTable not empty" << std::endl;
	tt.show();

}
#endif // TEST_TYPETABLE
