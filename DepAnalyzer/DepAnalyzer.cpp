/////////////////////////////////////////////////////////////////////
// DepAnalyzer.cpp -  Analyze Dependencied or fileset			   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////

#include<vector>
#include<string>
#include<fstream>
#include"DepAnalyzer.h"
#include"../FileSystem/FileSystem.h"
#include"../Tokenizer/Tokenizer.h"

using namespace std;
using namespace FileSystem;

// retrives all files in root for analysis
vector<string> DepAnalyzer::getFilesForAnal(string path)
{
	vector<string> files;
	vector<string> dirs = Directory::getDirectories(path);

	for (auto dir : dirs)
	{
		if (dir == ".." || dir == ".")
			continue;
		vector<string> subDirFiles;
		subDirFiles = getFilesForAnal(Path::fileSpec(path, dir));
		if (subDirFiles.size() != 0)
		{
			files.insert(files.end(), subDirFiles.begin(), subDirFiles.end());
		}
	}
	vector<string> cppFiles = Directory::getFiles(path, "*.cpp");
	vector<string> hFiles = Directory::getFiles(path, "*.h");

	for (size_t i = 0; i < cppFiles.size(); i++)
		cppFiles[i] = Path::fileSpec(path, cppFiles[i]);
	for (size_t i = 0; i < hFiles.size(); i++)
		hFiles[i] = Path::fileSpec(path, hFiles[i]);

	files.insert(files.end(), cppFiles.begin(), cppFiles.end());
	files.insert(files.end(), hFiles.begin(), hFiles.end());
	
	return files;
}

// Tokenizes onr file to find its dependencies
vector<string> DepAnalyzer::depAnalFile(std::string filePath)
{
	vector<string> fileDeps;
	Scanner::Toker tk;
	std::ifstream in(filePath);
	if (!in.good())
	{
		std::cout << "\n  can't open " << filePath << "\n\n";
		return fileDeps;
	}
	tk.attach(&in);
	//std::cout << "All Tokens in " << filePath << " :\n";
	do
	{
		std::string tok = tk.getTok();
		/*if (tok == "\n")
			tok = "newline";
		std::cout << "\n -- " << tok;*/
		vector<TypeInfo> tiValues = tt.getValues(tok);

		if (tiValues.size() > 0)
		{
			for (auto value : tiValues)
			{
				fileDeps.push_back(value.filename);
			}
		}
	} while (in.good());
	sort(fileDeps.begin(), fileDeps.end());
	fileDeps.erase(unique(fileDeps.begin(), fileDeps.end()), fileDeps.end());
	//fileDeps.erase(fileDeps.begin());
	return fileDeps;
}

// Removes self dependency 
vector<string> DepAnalyzer::rmvSelfDep(string file, vector<string> deps)
{
	string filename = Path::getName(file);
	vector<string>::iterator selfDep = find(deps.begin(), deps.end(), filename);
	if (selfDep != deps.end())
		deps.erase(selfDep);
	return deps;
}

// adds a file and its dependencies to the DB
void DepAnalyzer::addToDB(string file, vector<string> deps)
{
	Element<string> elm;
	elm.name = Path::getName(file);
	elm.category = (Path::getExt(file) == "h") ? "Header File" : "Implementaion File";
	for (auto d : deps)
		elm.addChild(d);
	elm.timeDate = timeString();
	elm.data = file;
	db.addElem(elm.name, elm);
}

// Prints contents of DepTable
void DepAnalyzer::show()
{
	cout << " ============   Dependency Table Contents   ============= 		- Req #5\n";
	for (auto pair : depTable)
	{
		cout << " File : "<< pair.first << "\n";
		cout << " Dependencies : \n";
		for (auto dep : pair.second)
			cout << "    " << dep << endl;
		cout << "----------------------------------------" << endl;
	}
	
}

// Creates Xml if contents of DepTable nad saves xml file
void DepAnalyzer::toXml(string filename)
{
	size_t nameSize = filename.find_last_of(".") - filename.find_last_of("/\\");
	filename = "../Dependencies-" + filename.substr(filename.find_last_of("/\\") + 1, nameSize)+".xml";
	using namespace XmlProcessing;
	using SPtr = std::shared_ptr<AbstractXmlElement>;
	std::string xml;
	XmlDocument doc;
	SPtr pRoot = makeTaggedElement("Dependencies");
	doc.docElement() = pRoot;
	for (auto dep : depTable)
	{
		string file = dep.first;
		vector<string> fileDeps = dep.second;
		SPtr fileTag = makeTaggedElement("File");
		SPtr fileNameTag = makeTaggedElement("FileName");
		fileNameTag->addChild(makeTextElement(file));
		fileTag->addChild(fileNameTag);
		SPtr DepTag = makeTaggedElement("Dependencies");
		for (auto fd : fileDeps)
			DepTag->addChild(makeTextElement(fd));
		fileTag->addChild(DepTag);
		pRoot->addChild(fileTag);
	}
	xml = doc.toString();
	std::ofstream fs(filename);
	fs << xml;
	fs.close();
	std::cout << "\n\nDependencies Xml saved to file: " << filename << "   - Req #7" << std::endl;
	xml.erase(std::remove(xml.begin(), xml.end(), '\n'), xml.end());
	xml.erase(std::remove(xml.begin(), xml.end(), ' '), xml.end());
	cout << " Dependencies Xml Contents :  \n" <<
		" (Removed newlines and spaces to reduce output size, check xml file for proper xml structure)\n\n";
	cout << xml << endl;
}

// Prints contents of the DB creates with contents of DepTable
void DepAnalyzer::showDB()
{
	cout << " ============  DataBase Contents  ==============\n";
	for (auto key : db.getKeys())
	{
		cout << key << " :";
		cout << db.value(key).show();
		cout << endl;
	}
}

// Gets all files ans runs depAnalFile, rmvselfDep, addToDB on each file and polulates DepTable
void DepAnalyzer::doDepAnal()
{
	//vector<string> files = getFilesForAnal(root);
	cout << "\n\n\n Starting Dependency Analysis : \n";
	vector<string> files;
	files = getFilesForAnal(root);
	for (auto file : files)
	{
		vector<string> fileDeps = depAnalFile(file);
		fileDeps = rmvSelfDep(file, fileDeps);
		addToDB(file, fileDeps);
		depTable[Path::getName(file)] = fileDeps;
	}
	
}

// returns the DepTable created by doDepAnal
std::unordered_map<std::string, std::vector<std::string>> DepAnalyzer::getDepTable()
{
	return depTable;
}

#ifdef TEST_DEPANALYZER
int main()
{
	TypeAnal ta;
	ta.doTypeAnal();
	TypeTable tt = ta.getTypeTable();
	DepAnalyzer da("../Tester", tt);
	da.doDepAnal();
	da.show();
	da.showDB();
	da.toXml("Tester-Dependencies");
}
#endif // TEST_DEPANALYZER
